#Pregunta1
def saludar(nombre):
    print("Hola", nombre)

saludar("Juan")

#Pregunta2
def calcular_promedio(numeros):
    total = sum(numeros)
    promedio = total / len(numeros)
    return promedio

numeros = [10, 69, 21, 5, 78, 311]

promedio_numeros = calcular_promedio(numeros)
print("El promedio de los números es:", promedio_numeros)

#Pregunta3
#Funcion de retorno
def suma(a, b):
    return a + b

resultado = suma(2, 9)
print("La suma es: ",resultado)

#funcion sin retorno
def saludar(nombre):
    print("Hola", nombre)

saludar("Pedro")
